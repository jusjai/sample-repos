## Step 1 (To run The index.html file)

Please download the file or just copy the contents of the file and use any text editor to paste and open the file using the web browser to see the output of the code.

## Step 2 (MIT License)

MIT license being most friendly to open source project developments by putting very limited restrictions on the public developers. This is the most popularly used license for open source development projects.